const fs = require('fs')
const data = JSON.parse(fs.readFileSync('./foodyo_output.json'))

const recommendations = data.body.Recommendations
let initStr = ''
const toAdd = '--'

for (let i = 0; i < recommendations.length; i++) {
  console.log(recommendations[i].RestaurantName);
  let menu = recommendations[i].menu

  for (let j = 0; j < menu.length; j++) {
    if (menu[j].type == 'sectionheader') {
      let children = menu[j].children;
      initStr = '>'
      repeateTask(children);
    }
  }
}

function repeateTask (children) {
  for (let i = 0; i < children.length; i++) {
      if (children[i].type == 'item') {
        printName(children[i]);
      }
  }
}

function printName (children) {
  if (children.selected == 1) {
    initStr = toAdd+''+initStr
    console.log(initStr+''+children.name)
    child = children.children
    for (let j = 0; j < child.length; j++) {
      if (child[j].selected == 1) {
        initStr = toAdd+''+initStr
        printName(child[j])
      }
    }
  }
}